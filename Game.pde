/*
 +============================+
 =        Platformer          =
 =          Eleuin            =
 =         Game.pde           =
 +============================+
 */

class Game{
    boolean hasInitialized;
    short level;
    
    void init(){
        hasInitialized = true;
    }
    
    void update(){
        if(player.isJumping == true){
            if(player.jumpTime < 32){
                player.y -= 2;
                player.jumpTime++;
            }
            else if(player.jumpTime < 64){
                player.y += 2;
                player.jumpTime++;
            }
            else{
                if(Key[0] != true){
                    player.isJumping = false;
                }
                player.jumpTime = 0;
            }
        }      
        if(lvl.isOnBlock(player.x, player.y, 32, 32, lvl.lX, lvl.lY, lvl.lW, lvl.lH) == false && player.isJumping == false){
            player.y += 2;
        }
        player.x += player.direction;
    }
    
    void display(){
        // Render the player on the screen
        rectMode(CORNER);
        stroke(64);
        fill(colour[0], colour[1], colour[2]);
        rect(player.x, player.y, 32, 32);
        
        // Render the blocks
        rectMode(CORNER);
        stroke(64);
        fill(64);
        rect((512 / 16) * 1, (480 / 15) * 10, (512 / 16) * 14, (480 / 15) * 1);
    }
}
