/*
 +============================+
 =        Platformer          =
 =          Eleuin            =
 =        Player.pde          =
 +============================+
 */

class Player{
    int x, y, xS, yS, jumpTime;
    short health;
    
    boolean isJumping;
    byte direction;
    
    public Player(){
        isJumping = false;
        jumpTime = 0;
        direction = 0;
        health = 20;
        x = 240;
        xS = 0;
        y = 224;
        yS = 0;
    }
}
