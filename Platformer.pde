/*
 +============================+
 =        Platformer          =
 =          Eleuin            =
 =      Platformer.pde        =
 =                            =
 +----------------------------+ 
 =          TODO              =
 =                            =
 = - Fix Player physics       =
 = - Do collision detection   =
 = - Write clean code for     =
 =   button                   =
 =                            =
 +----------------------------+
 =          BUGS              =
 =                            =
 = - Jumping goes wonky       =
 +----------------------------+
 =       NOTES TO SELF        =
 =                            =
 = - Bug check extensively    =
 = - Don't forget to          =
 =   git add/commit/push      =
 +============================+
 */

String gameState;
float[] colour = new float[3];
float[] colourSpeed = new float[3];

Player player = new Player();
Level lvl = new Level();

boolean[] Key = new boolean[3];

MainMenu mainMenu = new MainMenu();
HowToPlay howToPlay = new HowToPlay();
Game game = new Game();

void setup(){
    size(512, 480); // this leaves room for 16x15 32 pixel blocks
    frameRate(60);
    background(0);
    
    colour[0] = 255;
    colour[1] = 0;
    colour[2] = 255;
    colourSpeed[0] = 0;
    colourSpeed[1] = 0.5;
    colourSpeed[2] = -0.5;
    
    for(int i = 0; i < Key.length; i++)
        Key[i] = false;
    
    gameState = "Main Menu";
}

void draw(){
    background(colour[0], colour[1], colour[2]);
    
    // Background colour shifting
    if(colour[0] == 255 && colour[1] == 255){
        colourSpeed[0] = -0.5;
        colourSpeed[1] = 0;
        colourSpeed[2] = 0.5;
    }
    else if(colour[1] == 255 && colour[2] == 255){
        colourSpeed[0] = 0.5;
        colourSpeed[1] = -0.5;
        colourSpeed[2] = 0;
    }
    else if(colour[0] == 255 && colour[2] == 255){
        colourSpeed[0] = 0;
        colourSpeed[1] = 0.5;
        colourSpeed[2] = -0.5;
    }
    colour[0] += colourSpeed[0];
    colour[1] += colourSpeed[1];
    colour[2] += colourSpeed[2];
    
    if(gameState == "Game"){
        if (game.hasInitialized == false){
            game.init();
        }
        game.update();
        game.display();
    }
    else if(gameState == "Main Menu"){
        mainMenu.display();
    }
    else if(gameState == "How To Play"){
        howToPlay.display();
    }
}

void keyPressed(){
    if(key == 'w'){
        player.isJumping = true;
        Key[0] = true;
    }
    if(key == 'a'){
        player.direction = -2;
        Key[1] = true;
    }
    if(key == 'd'){
        player.direction = 2; 
        Key[2] = true;
    }
}

void keyReleased(){
    if(key == 'w'){
        Key[0] = false;
    }
    if(key == 'a'){
        player.direction = 0;
        Key[1] = false;
    }
    if(key == 'd'){
        player.direction = 0;
        Key[2] = false;
    }
}
