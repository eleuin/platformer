/*
 +============================+
 =        Platformer          =
 =          Eleuin            =
 =      Platformer.pde        =
 =                            =
 +----------------------------+ 
 =          TODO              =
 =                            =
 = - Fix Player physics       =
 = - Do collision detection   =
 = - Write clean code for     =
 =   button                   =
 =                            =
 +----------------------------+
 =          BUGS              =
 =                            =
 = - Jumping goes wonky       =
 +----------------------------+
 =       NOTES TO SELF        =
 =                            =
 = - Bug check extensively    =
 = - Don't forget to          =
 =   git add/commit/push      =
 +============================+
 */

String gameState;
float[] colour = new float[3];
float[] colourSpeed = new float[3];

Player player = new Player();
Level lvl = new Level();

boolean[] Key = new boolean[3];

MainMenu mainMenu = new MainMenu();
HowToPlay howToPlay = new HowToPlay();
Game game = new Game();

void setup(){
    size(512, 480); // this leaves room for 16x15 32 pixel blocks
    frameRate(60);
    background(0);
    
    colour[0] = 255;
    colour[1] = 0;
    colour[2] = 255;
    colourSpeed[0] = 0;
    colourSpeed[1] = 0.5;
    colourSpeed[2] = -0.5;
    
    for(int i = 0; i < Key.length; i++)
        Key[i] = false;
    
    gameState = "Main Menu";
}

void draw(){
    background(colour[0], colour[1], colour[2]);
    
    // Background colour shifting
    if(colour[0] == 255 && colour[1] == 255){
        colourSpeed[0] = -0.5;
        colourSpeed[1] = 0;
        colourSpeed[2] = 0.5;
    }
    else if(colour[1] == 255 && colour[2] == 255){
        colourSpeed[0] = 0.5;
        colourSpeed[1] = -0.5;
        colourSpeed[2] = 0;
    }
    else if(colour[0] == 255 && colour[2] == 255){
        colourSpeed[0] = 0;
        colourSpeed[1] = 0.5;
        colourSpeed[2] = -0.5;
    }
    colour[0] += colourSpeed[0];
    colour[1] += colourSpeed[1];
    colour[2] += colourSpeed[2];
    
    if(gameState == "Game"){
        if (game.hasInitialized == false){
            game.init();
        }
        game.update();
        game.display();
    }
    else if(gameState == "Main Menu"){
        mainMenu.display();
    }
    else if(gameState == "How To Play"){
        howToPlay.display();
    }
}

void keyPressed(){
    if(key == 'w'){
        player.isJumping = true;
        Key[0] = true;
    }
    if(key == 'a'){
        player.direction = -2;
        Key[1] = true;
    }
    if(key == 'd'){
        player.direction = 2; 
        Key[2] = true;
    }
}

void keyReleased(){
    if(key == 'w'){
        Key[0] = false;
    }
    if(key == 'a'){
        player.direction = 0;
        Key[1] = false;
    }
    if(key == 'd'){
        player.direction = 0;
        Key[2] = false;
    }
}
/*
 +============================+
 =        Platformer          =
 =          Eleuin            =
 =         Game.pde           =
 +============================+
 */

class Game{
    boolean hasInitialized;
    short level;
    
    void init(){
        hasInitialized = true;
    }
    
    void update(){
        if(player.isJumping == true){
            if(player.jumpTime < 32){
                player.y -= 2;
                player.jumpTime++;
            }
            else if(player.jumpTime < 64){
                player.y += 2;
                player.jumpTime++;
            }
            else{
                if(Key[0] != true){
                    player.isJumping = false;
                }
                player.jumpTime = 0;
            }
        }      
        if(lvl.isOnBlock(player.x, player.y, 32, 32, lvl.lX, lvl.lY, lvl.lW, lvl.lH) == false && player.isJumping == false){
            player.y += 2;
        }
        player.x += player.direction;
    }
    
    void display(){
        // Render the player on the screen
        rectMode(CORNER);
        stroke(64);
        fill(colour[0], colour[1], colour[2]);
        rect(player.x, player.y, 32, 32);
        
        // Render the blocks
        rectMode(CORNER);
        stroke(64);
        fill(64);
        rect((512 / 16) * 1, (480 / 15) * 10, (512 / 16) * 14, (480 / 15) * 1);
    }
}
/*
 +============================+
 =        Platformer          =
 =          Eleuin            =
 =       HowToPlay.pde        =
 +============================+
 */
 
class HowToPlay{
    void display(){
        rectMode(CENTER);
        textAlign(CENTER, CENTER);
        textSize(32);
        fill(64);
        text("How To Play", width / 2, height / 8);
        if (mouseX >= 272 && mouseX <= 496 && mouseY >= 375 && mouseY <= 465){
            stroke(64);
            fill(colour[0], colour[1], colour[2]);
            rect(width * 0.75, (height / 4) * 3.5, width / 2 - width / 16, height / 4 - height / 16);
            
            textSize(32);
            fill(64);
            text("Main Menu", width * 0.75, (height / 4) * 3.5);
            
            if(mousePressed == true){
                gameState = "Main Menu";
                mousePressed = false;
            }
        }
        else{
            stroke(255);
            fill(colour[0], colour[1], colour[2]);
            rect(width * 0.75, (height / 4) * 3.5, width / 2 - width / 16, height / 4 - height / 16);
            
            textSize(32);
            fill(255);
            text("Main Menu", width * 0.75, (height / 4) * 3.5);
        }
    } 
}
/*
 +============================+
 =        Platformer          =
 =          Eleuin            =
 =         Level.pde          =
 +============================+
 */

class Level{
    int lX, lY, lW, lH;
    
    public Level(){
        lX = (512 / 16) * 1;
        lY = (480 / 15) * 10;
        lW = (512 / 16) * 14;
        lH = (480 / 15) * 1;
    }
    
    boolean isOnBlock(int pX, int pY, int pW, int pH, int bX, int bY, int bW, int bH){
        if(pX + pW >= bX && pX <= bX + bW && pY + pH >= bY && pY + pH <= bY + 4){
            return true;
        }
        else{
            return false;
        }
    }
    boolean isColliding(int pX, int pY, int pW, int pH, int bX, int bY, int bW, int bH){
        if(pX + pW >= bX && pX <= bX + bW && pY + pH >= bY && pY <= bY + bH){
            return true;
        }
        else{
            return false;
        }
    }
}
/*
 +============================+
 =        Platformer          =
 =          Eleuin            =
 =       MainMenu.pde         =
 +============================+
 */

class MainMenu{
    void display(){
        rectMode(CENTER);
        
        textAlign(CENTER, CENTER);
        textSize(64);
        fill(64);
        text("./Platformer", width / 2, height / 4);
        
        if(mouseX >= 16 && mouseX <= 496 && mouseY >= 255 && mouseY <= 345){
            stroke(64);
            fill(colour[0], colour[1], colour[2]);
            rect(width / 2, (height / 4) * 2.5, width - width / 16, height / 4 - height / 16);
            textAlign(CENTER, CENTER);
            textSize(32);
            fill(64);
            text("Play Game", width / 2, (height / 4) * 2.5);
            stroke(255);
            fill(colour[0], colour[1], colour[2]);
            rect(width / 2, (height / 4) * 3.5, width - width / 16, height / 4 - height / 16);
            textAlign(CENTER, CENTER);
            textSize(32);
            fill(255);
            text("How To Play", width / 2, (height / 4) * 3.5);
            
            if(mousePressed == true){
                gameState = "Game";
            }
        }
        else if(mouseX >= 16 && mouseX <= 496 && mouseY >= 375 && mouseY <= 465){
            stroke(255);
            fill(colour[0], colour[1], colour[2]);
            rect(width / 2, (height / 4) * 2.5, width - width / 16, height / 4 - height / 16);
            textAlign(CENTER, CENTER);
            textSize(32);
            fill(255);
            text("Play Game", width / 2, (height / 4) * 2.5);
            stroke(64);
            fill(colour[0], colour[1], colour[2]);
            rect(width / 2, (height / 4) * 3.5, width - width / 16, height / 4 - height / 16);
            textAlign(CENTER, CENTER);
            textSize(32);
            fill(64);
            text("How To Play", width / 2, (height / 4) * 3.5);
            
            if(mousePressed == true){
                gameState = "How To Play";
                mousePressed = false;
            }
        }
        else{
            stroke(255);
            fill(colour[0], colour[1], colour[2]);
            rect(width / 2, (height / 4) * 2.5, width - width / 16, height / 4 - height / 16);
            rect(width / 2, (height / 4) * 3.5, width - width / 16, height / 4 - height / 16);
            textAlign(CENTER, CENTER);
            textSize(32);
            fill(255);
            text("Play Game", width / 2, (height / 4) * 2.5);
            text("How To Play", width / 2, (height / 4) * 3.5);
        }
    }
}
/*
 +============================+
 =        Platformer          =
 =          Eleuin            =
 =        Player.pde          =
 +============================+
 */

class Player{
    int x, y, xS, yS, jumpTime;
    short health;
    
    boolean isJumping;
    byte direction;
    
    public Player(){
        isJumping = false;
        jumpTime = 0;
        direction = 0;
        health = 20;
        x = 240;
        xS = 0;
        y = 224;
        yS = 0;
    }
}

