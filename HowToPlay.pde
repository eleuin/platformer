/*
 +============================+
 =        Platformer          =
 =          Eleuin            =
 =       HowToPlay.pde        =
 +============================+
 */
 
class HowToPlay{
    void display(){
        rectMode(CENTER);
        textAlign(CENTER, CENTER);
        textSize(32);
        fill(64);
        text("How To Play", width / 2, height / 8);
        if (mouseX >= 272 && mouseX <= 496 && mouseY >= 375 && mouseY <= 465){
            stroke(64);
            fill(colour[0], colour[1], colour[2]);
            rect(width * 0.75, (height / 4) * 3.5, width / 2 - width / 16, height / 4 - height / 16);
            
            textSize(32);
            fill(64);
            text("Main Menu", width * 0.75, (height / 4) * 3.5);
            
            if(mousePressed == true){
                gameState = "Main Menu";
                mousePressed = false;
            }
        }
        else{
            stroke(255);
            fill(colour[0], colour[1], colour[2]);
            rect(width * 0.75, (height / 4) * 3.5, width / 2 - width / 16, height / 4 - height / 16);
            
            textSize(32);
            fill(255);
            text("Main Menu", width * 0.75, (height / 4) * 3.5);
        }
    } 
}
