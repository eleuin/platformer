/*
 +============================+
 =        Platformer          =
 =          Eleuin            =
 =       MainMenu.pde         =
 +============================+
 */

class MainMenu{
    void display(){
        rectMode(CENTER);
        
        textAlign(CENTER, CENTER);
        textSize(64);
        fill(64);
        text("./Platformer", width / 2, height / 4);
        
        if(mouseX >= 16 && mouseX <= 496 && mouseY >= 255 && mouseY <= 345){
            stroke(64);
            fill(colour[0], colour[1], colour[2]);
            rect(width / 2, (height / 4) * 2.5, width - width / 16, height / 4 - height / 16);
            textAlign(CENTER, CENTER);
            textSize(32);
            fill(64);
            text("Play Game", width / 2, (height / 4) * 2.5);
            stroke(255);
            fill(colour[0], colour[1], colour[2]);
            rect(width / 2, (height / 4) * 3.5, width - width / 16, height / 4 - height / 16);
            textAlign(CENTER, CENTER);
            textSize(32);
            fill(255);
            text("How To Play", width / 2, (height / 4) * 3.5);
            
            if(mousePressed == true){
                gameState = "Game";
            }
        }
        else if(mouseX >= 16 && mouseX <= 496 && mouseY >= 375 && mouseY <= 465){
            stroke(255);
            fill(colour[0], colour[1], colour[2]);
            rect(width / 2, (height / 4) * 2.5, width - width / 16, height / 4 - height / 16);
            textAlign(CENTER, CENTER);
            textSize(32);
            fill(255);
            text("Play Game", width / 2, (height / 4) * 2.5);
            stroke(64);
            fill(colour[0], colour[1], colour[2]);
            rect(width / 2, (height / 4) * 3.5, width - width / 16, height / 4 - height / 16);
            textAlign(CENTER, CENTER);
            textSize(32);
            fill(64);
            text("How To Play", width / 2, (height / 4) * 3.5);
            
            if(mousePressed == true){
                gameState = "How To Play";
                mousePressed = false;
            }
        }
        else{
            stroke(255);
            fill(colour[0], colour[1], colour[2]);
            rect(width / 2, (height / 4) * 2.5, width - width / 16, height / 4 - height / 16);
            rect(width / 2, (height / 4) * 3.5, width - width / 16, height / 4 - height / 16);
            textAlign(CENTER, CENTER);
            textSize(32);
            fill(255);
            text("Play Game", width / 2, (height / 4) * 2.5);
            text("How To Play", width / 2, (height / 4) * 3.5);
        }
    }
}
