/*
 +============================+
 =        Platformer          =
 =          Eleuin            =
 =         Level.pde          =
 +============================+
 */

class Level{
    int lX, lY, lW, lH;
    
    public Level(){
        lX = (512 / 16) * 1;
        lY = (480 / 15) * 10;
        lW = (512 / 16) * 14;
        lH = (480 / 15) * 1;
    }
    
    boolean isOnBlock(int pX, int pY, int pW, int pH, int bX, int bY, int bW, int bH){
        if(pX + pW >= bX && pX <= bX + bW && pY + pH >= bY && pY + pH <= bY + 4){
            return true;
        }
        else{
            return false;
        }
    }
    boolean isColliding(int pX, int pY, int pW, int pH, int bX, int bY, int bW, int bH){
        if(pX + pW >= bX && pX <= bX + bW && pY + pH >= bY && pY <= bY + bH){
            return true;
        }
        else{
            return false;
        }
    }
}
